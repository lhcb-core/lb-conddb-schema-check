use crate::parser::CondFileKeys;
use anyhow::{anyhow, Context, Result};
use indicatif::{ParallelProgressIterator, ProgressBar, ProgressStyle};
use log::{log_enabled, trace};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use serde_yaml::from_reader;
use std::cmp::max;
use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use std::ffi::OsStr;
use std::fs::File;
use std::path::{Path, PathBuf};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Schema {
    #[serde(flatten)]
    entries: BTreeMap<String, BTreeSet<String>>,
}

#[derive(Debug, PartialEq, Eq)]
enum PathType {
    Group,
    ConditionFile,
    ConditionDir,
    Ignored,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum Change {
    AddedKey { path: String, key: String },
    RemovedKey { path: String, key: String },
    AddedFile { path: String },
    RemovedFile { path: String },
}

#[derive(Debug, Default)]
pub struct Changes(Vec<Change>);

#[derive(Debug, Default, Ord, PartialOrd, Eq, PartialEq)]
pub enum SeverityOfChange {
    #[default]
    Unchanged,
    Minor,
    Major,
}

impl Changes {
    pub fn severity(&self) -> SeverityOfChange {
        use SeverityOfChange::*;
        let mut severity = Unchanged;
        for change in &self.0 {
            severity = max(
                severity,
                match change {
                    Change::AddedKey { .. } => Minor,
                    Change::AddedFile { .. } => Minor,
                    _ => Major,
                },
            );
        }
        severity
    }
}

impl PathType {
    fn detect<P: AsRef<Path>>(path: P) -> Self {
        let path = path.as_ref();
        let ext = path.extension().unwrap_or("".as_ref());
        if path
            .file_name()
            .and_then(OsStr::to_str)
            .map(|name| name.starts_with('.'))
            .unwrap_or(false)
        {
            // ignore hidden files
            PathType::Ignored
        } else if ext == "yml" || ext == "yaml" {
            // entries ending by .yaml are condition files or directories
            if path.is_dir() {
                PathType::ConditionDir
            } else {
                // if it is not a dir we assume it is a file
                PathType::ConditionFile
            }
        } else if path.is_dir() {
            // a directory to group conditions my have any name
            PathType::Group
        } else {
            // we ignore a file (actually a non-dir) that doesn't end by `.yaml`
            PathType::Ignored
        }
    }
}

impl Schema {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self> {
        trace!("collecting conditions");
        let conditions = list_condition_paths(&path)?;
        trace!("found {} conditions", conditions.len());
        let files_to_process: Vec<_> = conditions.iter().flat_map(|(_, files)| files).collect();
        let n_of_files = files_to_process.len() as u64;
        trace!("with {} files to process", n_of_files);
        let processor = files_to_process.into_par_iter().map(|path| {
            let CondFileKeys(entries) = from_reader(File::open(path)?)
                .with_context(|| format!("failed to parse {}", path.display()))?;
            Ok((path, entries))
        });

        let processed_files: HashMap<_, _> = if log_enabled!(log::Level::Info) && cfg!(not(test)) {
            let style = ProgressStyle::with_template(
                "[{elapsed_precise}] parsing yaml {wide_bar} {pos}/{len}",
            )
            .unwrap();
            let progress_bar = ProgressBar::new(n_of_files).with_style(style);
            processor
                .progress_with(progress_bar)
                .collect::<Result<_>>()?
        } else {
            processor.collect::<Result<_>>()?
        };

        trace!("all files processed");
        let entries = conditions
            .iter()
            .map(|(k, e)| -> Result<_> {
                let keys = e
                    .iter()
                    .map(|p| Ok((p, &processed_files[p])))
                    .reduce(|a, b| {
                        if a.is_err() {
                            a
                        } else if b.is_err() {
                            b
                        } else if a.as_ref().unwrap().1 == b.as_ref().unwrap().1 {
                            a
                        } else {
                            Err(anyhow!(
                                "different schema between {:?} and {:?}",
                                a.unwrap().0,
                                b.unwrap().0
                            ))
                        }
                    });
                let keys = keys.transpose()?;
                let keys = keys.map_or_else(
                    || Err(anyhow!("empty condition {:?}", k)),
                    |(_, keys)| Ok(keys),
                )?;
                Ok((
                    k.to_string_lossy().to_string(),
                    keys.iter().map(String::from).collect(),
                ))
            })
            .collect::<Result<_>>()?;
        Ok(Self { entries })
    }

    pub fn changes_from(&self, reference: &Self) -> Changes {
        let all_keys: HashSet<_> = self
            .entries
            .keys()
            .chain(reference.entries.keys())
            .collect();
        let mut changes = Vec::new();
        for path in all_keys {
            if !self.entries.contains_key(path) {
                changes.push(Change::RemovedFile { path: path.clone() });
            } else if !reference.entries.contains_key(path) {
                changes.push(Change::AddedFile { path: path.clone() });
            } else {
                let self_keys = &self.entries[path];
                let ref_keys = &reference.entries[path];
                changes.extend(
                    self_keys
                        .difference(ref_keys)
                        .map(|k| Change::AddedKey {
                            path: path.clone(),
                            key: k.clone(),
                        })
                        .chain(ref_keys.difference(self_keys).map(|k| Change::RemovedKey {
                            path: path.clone(),
                            key: k.clone(),
                        })),
                );
            }
        }
        Changes(changes)
    }
}

fn path_name_is_number(path: &Path) -> bool {
    path.file_name()
        .and_then(|n| n.to_str())
        .map_or(false, |name| name.chars().all(|c| c.is_ascii_digit()))
}

/// Scan a path for all conditions and return a list of pairs (condition_path, associated_files).
fn list_condition_paths<P: AsRef<Path>>(path: P) -> Result<Vec<(PathBuf, HashSet<PathBuf>)>> {
    if PathType::detect(&path) == PathType::Ignored {
        // it's an error if the scan entry point should be ignored
        return Err(anyhow!("invalid path: {}", path.as_ref().display()));
    }
    let mut accumulator = Vec::new();
    let base = path.as_ref();

    list_condition_paths_acc(base.to_owned(), &mut accumulator)?;
    Ok(accumulator
        .into_iter()
        .map(|(p, s)| (p.strip_prefix(base).unwrap().to_path_buf(), s))
        .collect())
}

fn list_condition_paths_acc(
    top: PathBuf,
    paths: &mut Vec<(PathBuf, HashSet<PathBuf>)>,
) -> Result<()> {
    match PathType::detect(&top) {
        PathType::ConditionFile => {
            let p = top.canonicalize()?;
            paths.push((top, HashSet::from([p])))
        }
        PathType::ConditionDir => {
            let mut acc = HashSet::new();
            collect_from_cond_dir(&top, &mut acc)?;
            if acc.is_empty() {
                return Err(anyhow!(
                    "no schema found in condition dir {}",
                    top.display()
                ));
            }
            paths.push((top, acc))
        }
        PathType::Group => top.read_dir()?.try_for_each(|r| {
            r.map_err(anyhow::Error::new)
                .and_then(|e| list_condition_paths_acc(e.path(), paths))
        })?,
        PathType::Ignored => (),
    }
    Ok(())
}

fn collect_from_cond_dir(top: &Path, accumulator: &mut HashSet<PathBuf>) -> Result<()> {
    for entry in top.read_dir()? {
        let path = entry?.path();
        if path_name_is_number(&path) {
            if path.is_file() {
                accumulator.insert(path.canonicalize()?);
            } else if path.is_dir() {
                collect_from_cond_dir(&path, accumulator)?;
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn path_types() {
        assert_eq!(
            PathType::detect("test_dir/conditions_no_iov/group"),
            PathType::Group
        );
        assert_eq!(
            PathType::detect("test_dir/conditions_no_iov/a.yaml"),
            PathType::ConditionFile
        );
        assert_eq!(
            PathType::detect("test_dir/conditions_with_iov/a.yaml"),
            PathType::ConditionDir,
        );
        assert_eq!(
            PathType::detect("test_dir/conditions_no_iov/empty_group"),
            PathType::Group
        );
        assert_eq!(
            PathType::detect("test_dir/conditions_no_iov/empty_group/.empty"),
            PathType::Ignored,
        );
        assert_eq!(PathType::detect("does_not_exist"), PathType::Ignored);
        assert_eq!(PathType::detect("Cargo.toml"), PathType::Ignored);
    }

    #[test]
    fn schema_from_path() {
        assert!(Schema::from_path("test_dir/conditions_no_iov/z.yaml").is_err());

        let expected: Schema = serde_json::from_str(
            r#"{
                "a.yaml": ["a", "b"],
                "b.yml": ["b"],
                "group/c.yaml": ["cond1"],
                "group/d.yaml": ["item1", "item2"],
                "group/sub_group/e.yaml": ["e_value"]
            }"#,
        )
        .unwrap();
        assert_eq!(
            Schema::from_path("test_dir/conditions_no_iov").unwrap(),
            expected
        );

        let expected: Schema = serde_json::from_str(
            r#"{
                "a.yaml": ["a"],
                "grouped.yaml": ["cond"]
            }"#,
        )
        .unwrap();
        assert_eq!(
            Schema::from_path("test_dir/conditions_with_iov").unwrap(),
            expected
        );
    }

    mod error_cases {
        use crate::*;

        #[test]
        fn bad_yaml() {
            let err = Schema::from_path("test_dir/error_cases/bad_yaml.yaml");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("failed to parse"));
            assert!(msg.contains("bad_yaml.yaml"));
        }

        #[test]
        fn invalid_paths() {
            // no point in starting from an ignored file
            let err = Schema::from_path("test_dir/conditions_with_iov/a.yaml/.condition");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("invalid path"));
            assert!(msg.contains(".condition"));

            // no point in starting from a non `.yaml`
            let err = Schema::from_path("test_dir/conditions_with_iov/a.yaml/0");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("invalid path"));
            assert!(msg.contains('0'));
        }

        #[test]
        fn empty_condition_dir() {
            let err = Schema::from_path("test_dir/error_cases/empty.yaml");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("no schema found in condition dir"));
            assert!(msg.contains("empty.yaml"));
        }

        #[test]
        fn inconsistent_schema() {
            let err = Schema::from_path("test_dir/error_cases/inconsistent_schema/flat.yaml");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("different schema between"));
            assert!(msg.contains("/100"));

            let err = Schema::from_path("test_dir/error_cases/inconsistent_schema/grouped.yaml");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("different schema between"));
            assert!(msg.contains("/100"));

            let err = Schema::from_path("test_dir/error_cases/inconsistent_schema/in_group.yaml");
            assert!(err.is_err());
            let msg = err.unwrap_err().to_string();
            assert!(msg.starts_with("different schema between"));
            assert!(msg.contains("/100"));
        }
    }

    mod changes {
        use super::SeverityOfChange::*;
        use super::{Change, Schema};

        #[test]
        fn unchanged() {
            {
                let a = Schema::from_path("test_dir/conditions_no_iov").unwrap();
                let b = Schema::from_path("test_dir/conditions_no_iov").unwrap();
                let changes = a.changes_from(&b);
                assert!(changes.0.is_empty());
                assert_eq!(changes.severity(), Unchanged);
            }
            {
                let a = Schema::from_path("test_dir/conditions_with_iov").unwrap();
                let b = Schema::from_path("test_dir/conditions_with_iov").unwrap();
                let changes = a.changes_from(&b);
                assert!(changes.0.is_empty());
                assert_eq!(changes.severity(), Unchanged);
            }
        }

        #[test]
        fn changing_keys() {
            {
                let a: Schema = serde_json::from_str(r#"{"cond.yaml": ["a", "b"]}"#).unwrap();
                let b: Schema = serde_json::from_str(r#"{"cond.yaml": ["a"]}"#).unwrap();

                let changes = a.changes_from(&b);
                assert_eq!(changes.0.len(), 1);
                assert_eq!(
                    changes.0[0],
                    Change::AddedKey {
                        path: String::from("cond.yaml"),
                        key: String::from("b")
                    }
                );
                assert_eq!(changes.severity(), Minor);

                let changes = b.changes_from(&a);
                assert_eq!(changes.0.len(), 1);
                assert_eq!(
                    changes.0[0],
                    Change::RemovedKey {
                        path: String::from("cond.yaml"),
                        key: String::from("b")
                    }
                );
                assert_eq!(changes.severity(), Major);
            }
            {
                let a: Schema =
                    serde_json::from_str(r#"{"a.yaml": ["a"], "b.yaml": ["b"]}"#).unwrap();
                let b: Schema = serde_json::from_str(r#"{"a.yaml": ["a"]}"#).unwrap();
                let changes = a.changes_from(&b);
                assert_eq!(changes.0.len(), 1);
                assert_eq!(
                    changes.0[0],
                    Change::AddedFile {
                        path: String::from("b.yaml")
                    }
                );
                assert_eq!(changes.severity(), Minor);

                let changes = b.changes_from(&a);
                assert_eq!(changes.0.len(), 1);
                assert_eq!(
                    changes.0[0],
                    Change::RemovedFile {
                        path: String::from("b.yaml")
                    }
                );
                assert_eq!(changes.severity(), Major);
            }
        }
    }
}
