use serde::de::{IgnoredAny, MapAccess, Visitor};
use serde::{Deserialize, Deserializer};
use std::collections::HashSet;
use std::fmt::Formatter;
use std::marker::PhantomData;

// code heavily inspired by https://serde.rs/deserialize-map.html

pub struct CondFileKeys(pub HashSet<String>);

struct CondFileVisitor {
    marker: PhantomData<fn() -> CondFileKeys>,
}

impl CondFileVisitor {
    fn new() -> Self {
        CondFileVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de> Visitor<'de> for CondFileVisitor {
    type Value = CondFileKeys;
    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a map")
    }
    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut keys = HashSet::with_capacity(access.size_hint().unwrap_or(0));
        while let Some((key, _)) = access.next_entry::<String, IgnoredAny>()? {
            keys.insert(key);
        }
        Ok(CondFileKeys(keys))
    }
}

impl<'de> Deserialize<'de> for CondFileKeys {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(CondFileVisitor::new())
    }
}
