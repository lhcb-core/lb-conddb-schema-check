mod parser;
mod schema;

use anstream::println;
use anyhow::{Context, Result};
use clap::Parser;
use log::{debug, info, log_enabled, warn};
use owo_colors::OwoColorize;
use schema::{Schema, SeverityOfChange};
use std::fs::File;
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};

#[derive(Parser, Debug)]
#[command(version, author, about, long_about=None)]
struct Args {
    /// Path to the root directory to scan [default: current directory]
    base_dir: Option<PathBuf>,
    #[command(flatten)]
    verbose: clap_verbosity_flag::Verbosity<clap_verbosity_flag::InfoLevel>,
    /// Filename where to write the schema in JSON format
    #[clap(short, long)]
    output: Option<PathBuf>,
    /// If provided, compare the schema described in a reference file with that obtained from the
    /// scan of BASE_DIR [default: `<BASE_DIR>/.schema.json` if it exists]
    #[arg(short, long)]
    reference: Option<PathBuf>,
    /// Use as output destination the reference file path, equivalent to set
    /// the --output option to the same value as --reference option
    #[clap(short, long, default_value_t = false, conflicts_with = "output")]
    in_place: bool,
}

fn main() -> Result<()> {
    let args = Args::parse();
    env_logger::Builder::new()
        .filter_level(args.verbose.log_level_filter())
        .init();

    let base_dir = args.base_dir.map_or_else(std::env::current_dir, Ok)?;

    // try to get a reference schema
    let ref_file_name = args.reference.unwrap_or(base_dir.join(".schema.json"));
    let reference_schema = if ref_file_name.exists() {
        Some(read_reference_json(&ref_file_name).with_context(|| {
            format!(
                "failed to read reference schema from {}",
                ref_file_name.display()
            )
        })?)
    } else {
        warn!(
            "{} not found, schema comparison not available",
            ref_file_name.display()
        );
        None
    };

    info!("started analysis of {}", base_dir.display());
    let schema = Schema::from_path(base_dir)?;
    info!("analysis completed");

    let output = args.output.or(if args.in_place {
        Some(ref_file_name)
    } else {
        None
    });
    if let Some(path) = output {
        let mut output = File::create(path)?;
        serde_json::to_writer_pretty(&output, &schema)?;
        output.write_all(b"\n")?;
    } else {
        serde_json::to_writer_pretty(stdout(), &schema)?;
        stdout().write_all(b"\n")?;
    }

    if let Some(reference_schema) = reference_schema {
        let changes = schema.changes_from(&reference_schema);
        let severity = changes.severity();
        match severity {
            SeverityOfChange::Unchanged => {
                if log_enabled!(log::Level::Info) {
                    println!("{}", "no schema change detected".green());
                }
            }
            SeverityOfChange::Minor => println!("{}", "minor schema change detected".yellow()),
            SeverityOfChange::Major => println!("{}", "major schema change detected".red()),
        };
    }

    Ok(())
}

fn read_reference_json(path: &Path) -> Result<Schema> {
    debug!("using reference schema from {:?}", path);
    Ok(serde_json::from_reader(File::open(path)?)?)
}
