# Schema Check CLI for LHCb Conditions Database

A simple command line utility to extract and validate the schema of the
[LHCb Run3 Conditions Database](https://lhcb-conddb-run3.web.cern.ch/).
